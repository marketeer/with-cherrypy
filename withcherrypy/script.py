import random
import string
import cherrypy


class StringGenerator(object):
    @cherrypy.expose
    def index(self):
        return "Hello world! proband10011111"

    @cherrypy.expose
    def generate(self):
        return ''.join(random.sample(string.hexdigits, 8))


if __name__ == '__main__':
    cherrypy.server.socket_host = '0.0.0.0'
    cherrypy.quickstart(StringGenerator())
